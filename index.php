<?php
require 'credentials.php';
//Etape 1: Connexion à la base de données
try{
  $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
  $resultats = $dbh->query('SELECT * FROM utilisateurs');
}
catch(Exception $e){
  var_dump($e->getMessage());
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>index.php</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
  <?php require 'header.php';?>
  <div class="container">
    <div class="row">
      <h1 class="col-12 text-center">TABLEAU DES UTILISATEURS</h1>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>id</th>
              <th>nom</th>
              <th>prenom</th>
              <th>nom rue</th>
              <th>numéro rue</th>
              <th>code postale</th>
              <th>ville</th>
              <th>email</th>
              <th>sexe</th>
              <th>Mise à jour</th>
              <th>suppression</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($resultats as $value) {?>
            <tr>
              <td><?php echo $value['id']?></td>
              <td><?php echo $value['nom']?></td>
              <td><?php echo $value['prenom']?></td>
              <td><?php echo $value['nom_rue']?></td>
              <td><?php echo $value['num_rue']?></td>
              <td><?php echo $value['cp']?></td>
              <td><?php echo $value['ville']?></td>
              <td><?php echo $value['email']?></td>
              <td><?php echo $value['sexe']?></td>
              <td><a href="update.php?id=<?php echo $value["id"];?>"><button class="btn btn-primary">Update</button></td>
              <td>
                <form class="" action="traitement-suppression.php" method="post">
                    <input type="hidden" value="<?php echo $value['id']?>" name="Id">
                    <input type="submit" value="Supprimer" class="btn btn-danger"></td>
                </form>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
    </div>
  </div>

  <?php require 'footer.php'; ?>
</body>

</html>
