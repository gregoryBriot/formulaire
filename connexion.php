<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Se connecter</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  </head>
  <body>
    <?php
      require 'header.php';
     ?>
     <div class="container">
       <div class="row">
         <div class="col-md-6 m-auto">
           <h1 class="col-md-12 text-center">Se connecter</h1>
           <form class="form-group text-center" action="traitement-connexion.php" method="post">
              <label for="">Email</label>
              <input class="form-control" type="text" name="email" style="border:2px solid black">
              <label for="">Mot de passe</label>
              <input class="form-control mb-2" type="text" name="mdp" style="border:2px solid black">
              <input class="btn btn-primary" type="submit" name="" value="Se connecter">
          </form>
        </div>
    </div>
  </div>
  </body>
</html>
